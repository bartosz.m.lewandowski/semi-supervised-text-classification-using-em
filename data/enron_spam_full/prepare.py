import pandas as pd
from collections import Counter
import json
import random

df = pd.read_csv("enron_spam_data.csv")
df.fillna('', inplace=True)
print(df)
label2id = {'ham': 0, 'spam': 1}

rows = [{'message_id': row['Message ID'], 
         'text': (row['Subject']+" "+row['Message']).strip(),
         'label': label2id[row['Spam/Ham']],
         'label_text': row['Spam/Ham'],
         'subject': row['Subject'],
         'message': row['Message'],
         'date': row['Date']
         } for idx, row in df.iterrows()]

random.seed(42)
random.shuffle(rows)

num_test = 2000
splits = {'test': rows[0:num_test], 'train': rows[num_test:]}

print("Train:", len(splits['train']))
print("Test:", len(splits['test']))

num_spam = Counter()

for row in splits['test']:
    num_spam[row['label']] += 1
print(num_spam)

for split in ['train', 'test']:
    with open(f'{split}.jsonl', 'w') as fOut:
        for row in splits[split]:
            fOut.write(json.dumps(row)+"\n")
