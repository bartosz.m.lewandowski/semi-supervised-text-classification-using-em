import re

# tokenizing function with removing all-nonalphanumeric data
def tokenize(text: str, split: bool = False):
    
    assert isinstance(text, str)

    # set as low
    text = text.lower()
    # remove all non-alphanumeric characters
    text = re.sub(
        r"[^a-zA-Z 0-9]", " ", str(text)
    )  # przechodzac (od lewej) po ciagu znakow, te które nie zostały wskazane w 'pattern' zamienia na wskazana wartosc 'repl'
    # return iterable list of characters
    if split:
        return text.split()

    return text
