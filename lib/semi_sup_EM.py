import numpy as np
from lib.log_probability import log_prob

from scipy.sparse import vstack, hstack
from scipy.sparse import issparse

from sklearn.naive_bayes import MultinomialNB, _BaseDiscreteNB
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import check_X_y, check_array
from sklearn.utils.extmath import safe_sparse_dot
from sklearn.utils.validation import check_is_fitted
from sklearn.metrics import classification_report, confusion_matrix


def semi_sup_EM(
    X_labeled_, y_labeled_, X_ul_, X_test_, y_test_, alpha_=1.0, max_iter_=30
):
    assert all(isinstance(y, int) for y in y_labeled_), "Labels must be integers!"

    y_pred_hist = {}

    mNB = MultinomialNB(alpha=alpha_)
    mNB.fit(X_labeled_, y_labeled_)
    y_pred = mNB.predict(X_test_)
    cm = confusion_matrix(y_test_, y_pred)
    print("Confusion Matrix:\n", cm)
    print("Classification report:\n", classification_report(y_test_, y_pred))

    max_iter = max_iter_
    X_full = vstack((X_labeled_, X_ul_))

    # E-step
    y_E = mNB.predict(X_ul_)
    y_E_prob = mNB.predict_proba(X_ul_)
    # M-step
    y_new = hstack((np.array(y_labeled_), y_E)).toarray().ravel()
    mNB.fit(X_full, y_new)

    log_theta, log_theta_new = -np.inf, log_prob(
        X_ul_,
        X_labeled_,
        y_labeled_,
        mNB.feature_log_prob_,
        mNB.class_log_prior_,
        # y_E_prob,
    )
    print("Initial {}".format(log_theta_new))
    i = 0

    while log_theta < log_theta_new and i < max_iter:
        # measurement
        y_pred_hist[i] = (log_theta_new, y_pred)
        i += 1

        # E-step
        y_E = mNB.predict(X_ul_)
        y_E_prob = mNB.predict_proba(X_ul_)
        # M-step
        y_new = hstack((np.array(y_labeled_), y_E)).toarray().ravel()
        mNB.fit(X_full, y_new)

        log_theta, log_theta_new = log_theta_new, log_prob(
            X_ul_,
            X_labeled_,
            y_labeled_,
            mNB.feature_log_prob_,
            mNB.class_log_prior_,
            # y_E_prob,
        )

        # measurement
        y_pred = mNB.predict(X_test_)
        print("Iteration: {} - {}".format(i, log_theta_new))

    y_pred = mNB.predict(X_test_)
    cm = confusion_matrix(y_test_, y_pred)
    print("Confusion Matrix:\n", cm)
    print("Classification report:\n", classification_report(y_test_, y_pred))

    return mNB, y_pred_hist


# Text Classification from Labeled and Unlabeled Documents using EM,
# Machine Learning, 39, 103–134, 2000 Kluwer Academic Publisher
# "Subsection with method whereby the influence of the unlabeled data is modulated [...]."
class semi_sup_EM_sklearn(_BaseDiscreteNB):
    def __init__(
        self,
        alpha=1.0,
        beta=1.0,  # NEW
        fit_prior=True,
        class_prior=None,
        tol=1e-3,  # NEW
        max_iter=30,  # NEW
        prnt=False,  # NEW
    ):
        self.alpha = alpha
        self.beta = beta  # NEW
        self.fit_prior = fit_prior
        self.class_prior = class_prior
        self.tol = tol  # NEW
        self.max_iter = max_iter  # NEW
        self.prnt = prnt  # NEW

    def _count(self, X, Y, U_X=np.array([]), U_prob=np.array([])):
        # scikit-learn
        """Count and smooth feature occurrences."""
        if np.any((X.data if issparse(X) else X) < 0):
            raise ValueError("Input X must be non-negative")

        # NEW
        if U_X.shape[0] > 0:
            """Add unlabeled documents probabilities with influence parameter (0 <= Beta <= 1)."""
            self.feature_count_ += self.beta * safe_sparse_dot(U_prob.T, U_X)
            self.class_count_ += self.beta * U_prob.sum(axis=0)
        else:
            # scikit-learn
            self.feature_count_ = safe_sparse_dot(Y.T, X)
            self.class_count_ = Y.sum(axis=0)

    # scikit-learn
    def _update_feature_log_prob(self, alpha):
        """Apply smoothing to raw counts and recompute log probabilities"""
        smoothed_fc = self.feature_count_ + alpha
        smoothed_cc = smoothed_fc.sum(axis=1)

        self.feature_log_prob_ = np.log(smoothed_fc) - np.log(
            smoothed_cc.reshape(-1, 1)
        )

    # scikit-learn
    def _joint_log_likelihood(self, X):
        """Calculate the posterior log probability of the samples X"""
        check_is_fitted(self, "classes_")

        X = check_array(X, accept_sparse="csr")
        return safe_sparse_dot(X, self.feature_log_prob_.T) + self.class_log_prior_

    def fit_EM(self, X_l, y, X_u=None, X_test=None, sample_weight=None):
        # NEW
        X = vstack((X_l, X_u))
        if X_u != None:
            y = np.concatenate((y, np.full(X_u.shape[0], -1)), axis=0)
        # Unlabeled data are marked with -1
        unlabeled = np.flatnonzero(y == -1)
        labeled = np.setdiff1d(np.arange(len(y)), unlabeled)

        # scikit-learn
        """Fit semi-supervised Naive Bayes classifier according to X, y."""
        X, y = check_X_y(X, y, "csr")
        _, n_features = X.shape

        # scikit-learn
        labelbin = LabelBinarizer()
        Y = labelbin.fit_transform(y[labeled])
        self.classes_ = labelbin.classes_
        if Y.shape[1] == 1:
            Y = np.concatenate((1 - Y, Y), axis=1)

        # scikit-learn
        # LabelBinarizer().fit_transform() returns arrays with dtype=np.int64.
        # We convert it to np.float64 to support sample_weight consistently;
        # this means we also don't have to cast X to floating point
        Y = Y.astype(np.float64, copy=False)
        if sample_weight is not None:
            sample_weight = np.atleast_2d(sample_weight)
            Y *= check_array(sample_weight).T
        class_prior = self.class_prior

        # scikit-learn
        # Count raw events from data before updating the class log prior
        # and feature log probas
        alpha = self.alpha
        self._count(X[labeled], Y)
        self._update_feature_log_prob(alpha)
        self._update_class_log_prior(class_prior=class_prior)

        # NEW
        loss = self._joint_log_likelihood(X)
        sum_loss = loss.sum()
        y_pred_hist = {}
        prnt = self.prnt

        # NEW
        # Run EM algorithm
        if len(unlabeled) > 0:
            self.num_iter = 0
            if prnt:
                print("Step {}: loss = {:f}".format(self.num_iter, sum_loss))
            if X_test != None:
                y_test = self.predict(X_test)
                y_pred_hist[self.num_iter] = (sum_loss, y_test)

            while self.num_iter < self.max_iter:
                self.num_iter += 1
                prev_sum_loss = sum_loss

                # E-step
                prob = self.predict_proba(X[unlabeled])

                # M-step
                self._count(X[labeled], Y, X[unlabeled], prob)
                self._update_feature_log_prob(self.beta)
                self._update_class_log_prior(class_prior=class_prior)

                loss = self._joint_log_likelihood(X)
                sum_loss = loss.sum()
                if prnt:
                    print("Step {}: loss = {:f}".format(self.num_iter, sum_loss))
                if X_test != None:
                    y_test = self.predict(X_test)
                    y_pred_hist[self.num_iter] = (sum_loss, y_test)

                if self.num_iter > 1 and prev_sum_loss - sum_loss < self.tol:
                    break

        if len(y_pred_hist) > 0:
            return self, y_pred_hist
        else:
            return self
